Name:		profanity
Version:	0.5.1
Release:	1%{?dist}
Summary:	A console-based XMPP client written in C using ncurses
Group:		Applications/Internet

License:	GPLv3
URL:		http://profanity.im/
Source0:	http://profanity.im/%{name}-%{version}.tar.gz

BuildRequires:	glib2-devel >= 2.32
BuildRequires:	libstrophe-devel
BuildRequires:	libnotify-devel
BuildRequires:	libotr-devel
BuildRequires:	ncurses-devel
BuildRequires:	libcurl-devel
BuildRequires:	readline-devel
BuildRequires:	libuuid-devel

%description
Profanity is a console based XMPP client written in C using ncurses and
libstrophe, inspired by Irssi.

%package	devel
Summary:	Development files for %{name}
Requires:	%{name}%{?_isa} = %{version}-%{release}

%description	devel
The %{name}-devel package contains libraries and header files for developing
applications that use %{name}.


%prep
%autosetup


%build
%configure --disable-static
%make_build


%install
rm -rf %{buildroot}
%make_install
find %{buildroot} -name '*.la' -exec rm -f {} ';'


%files
%license LICENSE.txt
%doc 
%{_bindir}/*
%{_datadir}/%{name}/*
%{_mandir}/man1/*
%{_libdir}/*.so

%files devel
%license LICENSE.txt
%doc
%{_includedir}/*


%changelog
* Fri Jul 14 2017 Link Dupont <linkdupont@fedoraproject.org> - 0.5.1-1
- New upstream release

* Sat Oct 15 2016 Link Dupont <linkdupont@fedoraproject.org> - 0.5.0-1
- New upstream release
- Clean up spec file

* Mon Jan 4 2016 Link Dupont <linkdupont@fedoraproject.org> - 0.4.7-1
- New upstream version

* Fri Sep 18 2015 Link Dupont <linkdupont@fedoraproject.org> - 0.4.6-1
- Initial package

